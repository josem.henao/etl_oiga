CREATE TABLE tags (
	tag_id int(3) NOT NULL,
	tag varchar(120) NOT NULL,
		PRIMARY KEY (tag_id)
	) ENGINE=InnoDB;

CREATE TABLE ingredients (
	ingredient_id int(7) NOT NULL,
	ingredient varchar(60) NOT NULL,
		PRIMARY KEY (ingredient_id)
	) ENGINE=InnoDB;

CREATE TABLE steps (
	step_id int(10) NOT NULL,
	step varchar(300) NOT NULL,
		PRIMARY KEY (step_id)
	) ENGINE=InnoDB;

CREATE TABLE nutritions (
	nutrition_id int(7) NOT NULL,
	nutrition varchar(60) NOT NULL,
	value float(4),
		PRIMARY KEY (nutrition_id)
	) ENGINE=InnoDB;

CREATE TABLE users (
	user_id int(10) NOT NULL,
		PRIMARY KEY (user_id)
	) ENGINE=InnoDB;

CREATE TABLE recipes (
	recipe_id int(10) NOT NULL,
	name varchar(200) NOT NULL,
	minutes int(4),
	contributor_id int(7) NOT NULL,
	submitted date,
	description varchar(350),
		PRIMARY KEY (recipe_id)
		FOREIGN KEY (contributor_id) REFERENCES users(user_id)
	) ENGINE=InnoDB;

CREATE TABLE interactions (
	interaction_id int(12) NOT NULL,
	user_id int(10) NOT NULL,
	recipe_id int(10),
	`date` date,
	rating float(4),
	review varchar(600) NOT NULL,
		PRIMARY KEY (interaction_id)
		FOREIGN KEY (user_id) REFERENCES users (user_id)
		FOREIGN KEY (recipe_id) REFERENCES recipes (recipe_id)
	) ENGINE=InnoDB;

CREATE TABLE recipes_nutritions (
	recipe_id int(10) NOT NULL,
	nutrition_id int(7) NOT NULL,
		FOREIGN KEY (recipe_id) REFERENCES recipes (recipe_id)
		FOREIGN KEY (nutrition_id) REFERENCES nutritions (nutrition_id)
	) ENGINE=InnoDB;


CREATE TABLE recipes_tags (
	recipe_id int(10) NOT NULL,
	tag_id int(7) NOT NULL,
		FOREIGN KEY (recipe_id) REFERENCES recipes (recipe_id)
		FOREIGN KEY (tag_id) REFERENCES tags (tag_id)
	) ENGINE=InnoDB;

CREATE TABLE recipes_ingredients (
	recipe_id int(10) NOT NULL,
	ingredient_id int(7) NOT NULL,
		FOREIGN KEY (recipe_id) REFERENCES recipes (recipe_id)
		FOREIGN KEY (ingredient_id) REFERENCES ingredients (ingredient_id)
	) ENGINE=InnoDB;

CREATE TABLE recipes_steps (
	recipe_id int(10) NOT NULL,
	step_id int(7) NOT NULL,
		FOREIGN KEY (recipe_id) REFERENCES recipes (recipe_id)
		FOREIGN KEY (step_id) REFERENCES steps (step_id)
	) ENGINE=InnoDB;
