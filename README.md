Proyecto para procesar un ETL sobre datos de recetas y la interación de usuarios con estas. Las recetas tienen
una série de relaciones con otro tipo de objetos los cuales se presentan
a continuación:

```
Recipe:
    - id_recipe: int
    - name: str
    - minutes: int
    - contributor_id: int

User:
    - user_id: int

Interaction:
    - user_id: int
    - recipe_id: int
    - date: Datetime
    - rating: float
    - review: str

Tag:
    - tag_id:int

Step:
    - step_id: int
    - step: str

Ingredient:
    - ingredient_id: int
    - ingredient: str
 
```

El proyecto se compone principalmente de tres capapas de responsabilidad:

- ``Extracter``: Extrae los datos de los archivos originales y los limpia de 
algunos caracteres como finales de linea (`\n`). La mayor parte de los
datos obtenidos se han dajeado en su tipo de dato original: `str`.
El formato entregado por este componente es como se sigue:
```
dataset_name = [
  {
    'col_1': 'val_1',
    'col_2': 'val_2',
    ...
  },{
    'col_1': 'val_1',
    'col_2': 'val_2',
    ...
  },
  ...
]
```

- `Transformer`: A partir de los datos extraídos este componente se encarga
de aplicar un tipado a los datos de acuerdo a cada uno de los datasets originales.
El formato entregado por este componente entrega los datos de la sigiente forma:

```
dataset_name = [
  {
    'col_1': 'val_1':str,
    'col_2': val_2:int,
    ...
  },{
    'col_1': val_1:list_int,
    'col_2': val_2:float,
    ...
  },
  ...
]
```

- ``Loader``: Este componente se encarga de relacionar loas datos y encontrar aquellas relaciones 
entre todos los conjuntos de datos de modo que se pueda caragar a una base de datos como MySQL
de forma relacional.

Uno de los componentes secundarios es el ``respository``, este componentese encarga de abrir una
conexión a la base de datos y exponer una interfaz para realizar commits sobre la base de datos
elegida y además cerrar la conexión cuando se termine una transacción.