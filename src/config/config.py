import pymysql

DB_CONFIG = {
    'host': 'localhost',
    'user': 'root',
    'password': 'admin123',
    'db': 'oiga_warehouse',
    'charset': 'utf8mb4',
    'cursorclass': pymysql.cursors.DictCursor
}
