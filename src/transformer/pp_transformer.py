from src.transformer.util.util_transformer import set_value_types


def transform_pp(pp):
    print('--  Trasforming pp  --')
    PP_recipes = pp.get('PP_recipes')
    PP_users = pp.get('PP_users')

    set_value_types(PP_recipes,
                    ['int', 'int', 'list_int', 'list_list_int', 'list_int', 'list_int', 'int', 'list_int'])
    set_value_types(PP_users,
                    ['int', 'list_int', 'list_int', 'int', 'list_float', 'int'])

    print('--  PP transformed  --')
    return{
        'PP_recipes': PP_recipes,
        'PP_users': PP_users,
    }
