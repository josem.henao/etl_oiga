import datetime


def add_colum(data, column_name='target', value='train'):
    """
    This process updates the value of each object element in the data list appening a new colum called 'target' with
    his target label ('train', 'test' or 'validation') 

    :param data: Each sample in the Data
    :param column_name: The new column to add
    :param value: the valur for the new column
    :return: None
    """
    for r in data:
        r.update({column_name: value})


def set_value_types(data: list, data_types: list):
    """
    Set the data Type for each value depending on the data_types list especification for each column
    :param data: The data to be transformed
    :param data_types: A list of the data Type for each column
    :return: None
    """
    for i in range(len(data)):
        for k, dt in zip(data[i].keys(), data_types):
            if dt == 'int':
                data[i][k] = int(data[i][k])
            elif dt == 'float':
                data[i][k] = float(data[i][k])
            elif dt == 'str':
                data[i][k] = str(data[i][k])
            elif dt == 'date':
                data[i][k] = datetime.date(*list(map(lambda x: int(x), data[i][k].split('-'))))
            elif dt == 'list_str':
                data[i][k] = data[i][k][2:-2].split("', '")
            elif dt == 'list_float':
                data[i][k] = list(map(lambda x: float(x), data[i][k][1:-1].split(", ")))
            elif dt == 'list_int':
                data[i][k] = list(map(lambda x: int(x), data[i][k][1:-1].split(", ")))
            elif dt == 'list_list_int':
                s = data[i][k][2:-2].split("], [")
                s = list(map(lambda x: x.split(', '), s))
                data[i][k] = [[int(e) for e in row] for row in s]
            else:
                print(f'Data with an unsupported formatter: {data[i][k]}')


# def transform_types_with_objects(data: list, data_types: list):
#     """
#     Transform objects given in strings into his respective object indicated in data_types list
#     :param data: The data to be transformed
#     :param data_types: A list of the data Type for each object in the column
#     :return: None
#     """
#     for i in range(len(data)):
#         for k, dt in zip(data[i].keys(), data_types):
#             if dt == 'int':
#                 data[i][k] = int(data[i][k])
#             elif dt == 'float':
#                 data[i][k] = float(data[i][k])
#             elif dt == 'date':
#                 data[i][k] = datetime.date(*list(map(lambda x: int(x), data[i][k].split('-'))))
#             elif dt == 'list_str':
#                 data[i][k] = data[i][k][2:-2].split("', '")
#             elif dt == 'list_float':
#                 data[i][k] = list(map(lambda x: float(x), data[i][k][1:-1].split(", ")))
#             elif dt == 'list_int':
#                 data[i][k] = list(map(lambda x: int(x), data[i][k][1:-1].split(", ")))
#             elif dt == 'list_list_int':
#                 s = data[i][k][2:-2].split("], [")
#                 s = list(map(lambda x: x.split(', '), s))
#                 data[i][k] = [[int(e) for e in row] for row in s]
#             else:
#                 print(f'Data with an unsupported formatter: {data[i][k]}')
