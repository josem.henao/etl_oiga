from src.transformer.util.util_transformer import add_colum, set_value_types


def trasform_interactions(interactions):
    """
    Trasforma los datos de interactions que se entregan, aplica primero una transformacion a cada dataset (test,
    train y validation) agregando una columna `target` para indicar a que dataset se pertenece. Finalmente se juntan los
    datasets en uno solo y se aplica la transformacion de tipado

    :param interactions: Data to be transformed
    :return: None
    """
    print('--  Trasforming interactions  --')
    interactions_test = interactions.get('interactions_test')
    interactions_train = interactions.get('interactions_train')
    interactions_validation = interactions.get('interactions_validation')

    add_colum(interactions_test, 'target', 'test')
    add_colum(interactions_train, 'target', 'train')
    add_colum(interactions_validation, 'target', 'validation')

    interaction_data = interactions_test + interactions_train + interactions_validation

    del interactions_test, interactions_train, interactions_validation

    # {'user_id': '8937', 'recipe_id': '44551', 'date': '2005-12-23', 'rating': '4.0', 'u': '2', 'i': '173538',
    # 'target': 'test'}
    set_value_types(interaction_data, ['int', 'int', 'date', 'float', 'int', 'int', 'str'])
    print('--  Interactions transformed  --')
    return {
        'interaction_data': interaction_data,
    }
