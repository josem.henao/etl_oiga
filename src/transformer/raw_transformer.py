from src.transformer.util.util_transformer import set_value_types


def transform_raw(raw):
    print('--  Trasforming raw  --')
    RAW_interactions = raw.get('RAW_interactions')
    RAW_recipes = raw.get('RAW_recipes')

    # user_id, recipe_id, date, rating, review
    set_value_types(RAW_interactions,
                    ['int', 'int', 'date', 'float', 'str'])
    # name, id, minutes, contributor_id, submitted, tags, nutrition, n_steps, steps, description, ingredients,
    # n_ingredients
    set_value_types(RAW_recipes,
                    ['str', 'int', 'int', 'int', 'date', 'list_str', 'list_float', 'int', 'list_str', 'str', 'list_str'])

    print('--  RAW transformed  --')
    return{
        'RAW_interactions': RAW_interactions,
        'RAW_recipes': RAW_recipes,
    }