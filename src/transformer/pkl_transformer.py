from src.transformer.util.util_transformer import set_value_types


def transform_pkl(pkl):
    print('--  Trasforming PKL  --')
    ingr_map = pkl.get('ingr_map')

    # raw_ingr, raw_ingr, processed, len_proc, replaced, count, id
    set_value_types(ingr_map,
                    ['str', 'int', 'str', 'int', 'str', 'int', 'int'])

    print('--  PKL transformed  --')
    return {
        'ingr_map': ingr_map,
    }
