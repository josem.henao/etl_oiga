from abc import ABC
from src.config.config import DB_CONFIG
import pymysql


class MySQLEngine(ABC):
    _connection = None

    @classmethod
    def create_engine(cls):
        cls._connection = pymysql.connect(**DB_CONFIG)
        print(f"connection created {cls._connection}")

    @classmethod
    def get_connection(cls):
        cls.create_engine()
        return cls._connection

    @classmethod
    def commit_and_close_connection(cls):
        if cls._connection is not None:
            cls._connection.commit()
            cls._connection.close()
        else:
            raise Exception("No se tiene una sesion abierta para hacer commit y cerrar")

    @classmethod
    def commit_connection(cls):
        if cls._connection is not None:
            cls._connection.commit()
        else:
            raise Exception("No se tiene una sesion abierta para hacer commit")

    @classmethod
    def close_connection(cls):
        if cls._connection is not None:
            cls._connection.close()
        else:
            raise Exception("No se tiene una sesion abierta para cerrar")




