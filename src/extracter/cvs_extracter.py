import pickle
from abc import ABC
import pandas as pd


class CsvExtracter(ABC):
    @classmethod
    def get_content(cls, file_path):
        try:
            with open(file_path, 'r') as f:
                columns = f.readline()[:-1].split(',')
                rows = f.readlines()
                rows = list(map(lambda x: x[:-1], rows))  # Eliminanos el caracter \n
                rows = list(map(lambda x: x.split(','), rows))  # Dividomos por el caracter ','
                data = []
                for row in rows:
                    row_d = {}
                    for k, v in zip(columns, row):
                        row_d.update({k: v})
                    data.append(row_d)
                return data
        except FileNotFoundError:
            print(f"No se ha encontrado el archivo {file_path}")

    @classmethod
    def get_content_with_objects(cls, file_path):
        try:
            df = pd.read_csv(file_path)
            columns = list(df.columns)
            rows = list(map(lambda x: list(x), df.values))
            data = []
            for row in rows:
                row_d = {}
                for k, v in zip(columns, row):
                    row_d.update({k: v})
                data.append(row_d)
            return data

        except FileNotFoundError:
            print(f"No se ha encontrado el archivo {file_path}")

    @classmethod
    def get_pkl_content(cls, file_path):
        try:
            with open('file_path', 'rb') as f:
                df = pickle.load(f)
                columns = df.columns
                rows = df.df.values
                data = []
                for row in rows:
                    row_d = {}
                    for k, v in zip(columns, row):
                        row_d.update({k: v})
                    data.append(row_d)
                return data
        except FileNotFoundError:
            print(f"No se ha encontrado el archivo {file_path}")
