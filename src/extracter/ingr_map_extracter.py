from src.extracter.pkl_extracter import PklExtracter


def extract_ingr_map():
    print("--  Extracting ingr_map  --")
    ingr_map = PklExtracter.get_pkl_content('../data/ingr_map.pkl')
    print("--  Ingr_map extracted  --")
    return {
        'ingr_map': ingr_map,
    }
