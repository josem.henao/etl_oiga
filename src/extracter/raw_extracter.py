from src.extracter.cvs_extracter import CsvExtracter


def extract_raw():
    print("--  Extracting RAW  --")
    RAW_interactions = CsvExtracter.get_content_with_objects('../data/RAW_interactions.csv')
    RAW_recipes = CsvExtracter.get_content_with_objects('../data/RAW_recipes.csv')
    print("--  RAW extracted  --")
    return {
        'RAW_interactions': RAW_interactions,
        'RAW_recipes': RAW_recipes,
    }