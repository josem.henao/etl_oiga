import pickle
from abc import ABC


class PklExtracter(ABC):
    @classmethod
    def get_pkl_content(cls, file_path):
        try:
            with open(file_path, 'rb') as f:
                data = pickle.load(f)
                columns = list(data.columns)
                rows = list(data.values)
                data = []
                for row in rows:
                    row_d = {}
                    for k, v in zip(columns, row):
                        row_d.update({k: v})
                    data.append(row_d)
                return data
        except FileNotFoundError:
            print(f"No se ha encontrado el archivo {file_path}")
