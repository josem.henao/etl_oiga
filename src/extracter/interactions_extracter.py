from src.extracter.cvs_extracter import CsvExtracter


def extract_interactions():
    print("--  Extracting interactions  --")
    interactions_test = CsvExtracter.get_content('../data/interactions_test.csv')
    interactions_train = CsvExtracter.get_content('../data/interactions_train.csv')
    interactions_validation = CsvExtracter.get_content('../data/interactions_validation.csv')
    print("--  Interactions extracted  --")
    return {
        'interactions_test': interactions_test,
        'interactions_train': interactions_train,
        'interactions_validation': interactions_validation,
    }
