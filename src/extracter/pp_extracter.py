from src.extracter.cvs_extracter import CsvExtracter


def extract_pp():
    print("--  Extracting pp  --")
    PP_recipes = CsvExtracter.get_content_with_objects('../data/PP_recipes.csv')
    PP_users = CsvExtracter.get_content_with_objects('../data/PP_users.csv')
    print("--  PP extracted  --")
    return {
        'PP_recipes': PP_recipes,
        'PP_users': PP_users,
    }