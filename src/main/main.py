import time

from src.extracter.ingr_map_extracter import extract_ingr_map
from src.extracter.interactions_extracter import extract_interactions
from src.extracter.pp_extracter import extract_pp
from src.extracter.raw_extracter import extract_raw
from src.transformer.interaction_transformer import trasform_interactions
from src.transformer.pkl_transformer import transform_pkl
from src.transformer.pp_transformer import transform_pp
from src.transformer.raw_transformer import transform_raw


def main(*args, **kwargs):

    ####      INTERACTION DATA     #####
    interactions_extracted = extract_interactions()
    interactions_transformed = trasform_interactions(interactions_extracted)
    print("interactions keys: ", interactions_transformed.get('interaction_data')[0].keys())
    # TODO: Implement the Loader component for interaction data

    ####      PP DATA     ####
    PP_extracted = extract_pp()
    PP_transformed = transform_pp(PP_extracted)
    print("PP_recipes keys: ", PP_transformed.get('PP_recipes')[0].keys())
    print("PP_users keys", PP_transformed.get('PP_users')[0].keys())
    # TODO: Implement the Loader component for pp data

    ####      RAW DATA     ####
    RAW_extracted = extract_raw()
    RAW_transformed = transform_raw(RAW_extracted)
    print("RAW_interactions keys: ", RAW_transformed.get('RAW_interactions')[0].keys())
    print("RAW_recipes keys", RAW_transformed.get('RAW_recipes')[0].keys())
    # TODO: Implement the Loader component for raw data

    ####      PKL DATA      ####
    ingr_map_extracted = extract_ingr_map()
    ingr_map_transformed = transform_pkl(ingr_map_extracted)
    print("ingr_map keys: ", ingr_map_transformed.get('ingr_map')[0].keys())
    # TODO: Implement the Loader component for raw data


if __name__ == '__main__':
    start = time.time()
    main()
    end = time.time()
    print(f"\nProcessed in {round(end - start, 2)} seconds")